package org.simple.server;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.FileOutputStream;
import java.util.concurrent.atomic.AtomicBoolean;


public class SimpleFiles {
    private static String DEST_DIR = "/tmp";
    String fileName;

    public SimpleFiles(String fileName) {
        fileName = fileName;
    }

    public boolean isExist() {
        File currentFile = new File(DEST_DIR + this.fileName);
        return 	currentFile.exists();
    }

    public boolean writeFile(InputStream inputStream) {
        byte[] buffer = new byte[4096];
        int lengthRead;
        int lengthTotal = 0;
        File currentFile = new File(DEST_DIR + this.fileName);
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(currentFile, false);
            while ((lengthRead = inputStream.read(buffer, 0, 4096)) > 0)
            {
                fileOutputStream.write(buffer, 0, lengthRead);
                lengthTotal += lengthRead;
            }
            fileOutputStream.close();
            return true;
        } catch (IOException e) {
            return false;
        }
    }
}



